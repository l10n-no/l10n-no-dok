#!/bin/sh
java -jar ~/utvikling/saxon/saxon8.jar -s sprak.xml stilsett/sprak-til-po.xsl målspråk=nn > nn/sprak.po
java -jar ~/utvikling/saxon/saxon8.jar -s sprak.xml stilsett/sprak-til-po.xsl målspråk=nb > nb/sprak.po

java -jar ~/utvikling/saxon/saxon8.jar -s land.xml stilsett/land-til-po.xsl målspråk=nn > nn/land.po
java -jar ~/utvikling/saxon/saxon8.jar -s land.xml stilsett/land-til-po.xsl målspråk=nb > nb/land.po

<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="utf-8" indent="no"
            method="text"
            omit-xml-declaration="yes"
            doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
            doctype-system="http://www.w3.org/TR/html4/loose.dtd"/> 

<!-- Vel målspråk - nynorsk («nn») eller bokmål («nb») -->
<!-- Standard er fransk («fr»)! -->
<xsl:param name="målspråk" select="'fr'"/>

<xsl:strip-space elements="*"/>

<xsl:template match="språksamling">

# Språknamn på engelsk og norsk
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Karl Ove Hufthammer &lt;karl@huftis.org>\n"
"Language-Team: Norwegian <xsl:choose>
  <xsl:when test="$målspråk='nn'">Nynorsk</xsl:when>
  <xsl:when test="$målspråk='nb'">Bokmål</xsl:when>
  <xsl:otherwise>*FEIL*</xsl:otherwise>
</xsl:choose> &lt;l10n-no@lister.huftis.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

<xsl:for-each select="(språk/en[not(alt)])|(språk/en/alt)">
<xsl:sort order="ascending" data-type="text" select="."/>
<xsl:variable name="omsetjing" select="ancestor::språk/node()[local-name() = $målspråk]"/>
<xsl:if test="contains($omsetjing, '*')">&#xa;#, fuzzy</xsl:if>
msgid "<xsl:value-of select="."/>"
msgstr "<xsl:call-template name="lagliste"><xsl:with-param name="omsetjing" select="$omsetjing"/></xsl:call-template>"
</xsl:for-each>

</xsl:template>

<xsl:template name="lagliste">
<xsl:param name="omsetjing"/>
<xsl:for-each select="$omsetjing">
  <xsl:text/><xsl:value-of select="concat(alt[1], text())"/><xsl:text/>
  <xsl:text/><xsl:for-each select="alt[position() != 1]">; <xsl:value-of select="."/></xsl:for-each><xsl:text/>
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>

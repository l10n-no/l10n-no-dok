<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="utf-8" indent="no"
            method="text"/> 

<xsl:strip-space elements="*"/>

<xsl:template match="landsamling">
---
title: Landskodar og landsnamn
author: Karl Ove Hufthammer
description: "Oversikt over norske namn (nynorsk og bokmål) på alle landa i den internasjonale standarden ISO 3166."
lang: nn
---

Dette dokumentet inneheld oversikt over alle landa i
<a href="https://www.iso.org/obp/ui/#search"><abbr lang="en" title="International Organization for Standardization">ISO</abbr>&#xa0;3166-1</a>,
med tilhøyrande landskodar.

Landsnamnet på bokmål er berre nemnt
viss dette er forskjellig for det nynorske landsnamnet. I den underliggjande
databasen er derimot alle kodar og namn eksplisitt nemnt. Ikkje rediger
dette dokumentet, men heller den underliggjande databasen. Les
kommentarane i denne for meir informasjon.

Landa er sortert etter dei engelske landsnamna. Nokre landsnamn har fleire
variantar på norsk, skriven som ei nummerert og prioritert liste. Du skal
bruka det første namnet i lista.

Kvalitet er mykje viktigare enn kvantitet. Alle omsetjingane
her skal vera korrekte, og er henta frå offisielle kjelder:

* [Språkrådet&#xa0;&#8211; geografiliste](https://sprakradet.no/stedsnavn-og-navn-pa-statsorgan/navnelister-norsk-skrivemate/utanlandske-stadnamn-navn-pa-stater-og-sprak-transkripsjon/)
* [Språkrådet&#xa0;&#8211; rettskrivingsendringer frå 1.&#xa0;juli 2005](https://sprakradet.no/wp-content/uploads/rettskriving2005.pdf)
* [Utanriksdepartementet](https://www.regjeringen.no/no/dokumentarkiv/regjeringen-stoere/andre-dokumenter/ud/statsnavn/statsnavn-hovedsteder-og-nasjonaldager/id87863/)
* Privat korrespondanse med Språkrådet

Teiknet «*» er med i namn som ikkje har nokon
kjend offisiell skrivemåte. Skrivemåten er altså ikkje henta
frå kjeldene ovanfor, men frå andre (gode) kjelder.

Kommentarar kan sendast til meg, [Karl Ove Hufthammer](mailto:karl@huftis.org).

Her er statusen for omsetjinga:

Oppdatert per &lt;abbr lang="en">ISO&lt;/abbr>&#xa0;3166
  ~ <xsl:value-of select="@iso3166-utgåve"/>

Land totalt
  ~ <xsl:value-of select="count(land)"/>

Omsett til norsk
  ~ <xsl:value-of select="count(land/nn)"/> (<xsl:number format="1" value="100 * count(land/nn) div count(land)"/>&#xa0;%)

&lt;table class="table">

&lt;thead>
&lt;tr>
  &lt;th>&lt;abbr lang="en">ISO&lt;/abbr> 3166-1-alpha-1&lt;/th>
  &lt;th>Engelsk&lt;/th>
  &lt;th>Fransk&lt;/th>
  &lt;th>Nynorsk&lt;/th>
  &lt;th>Bokmål&lt;/th>
&lt;/tr>
&lt;/thead>

&lt;tbody>
<xsl:for-each select="land">
<xsl:sort order="ascending" data-type="text" select="en"/>
&lt;tr>
&lt;td><xsl:value-of select="iso3166-1-alpha-2"/>&lt;/td>
&lt;td><xsl:apply-templates select="en"/>&lt;/td>
&lt;td><xsl:apply-templates select="fr"/>&lt;/td>
&lt;td><xsl:apply-templates select="nn"/><xsl:if test="not(nn)">&#8211;</xsl:if>&lt;/td>
&lt;td><xsl:choose>
    <xsl:when test="not(nb) or (nb)=(nn)">&#8211;</xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="nb"/>
    </xsl:otherwise>
  </xsl:choose>&lt;/td>
&lt;/tr>
</xsl:for-each>
&lt;/tbody>

&lt;/table>
</xsl:template>

<xsl:template match="fr|nn|nb">
<xsl:choose>
  <xsl:when test="alt">
    <xsl:for-each select="alt"><!--
    -->1. <xsl:value-of select="."/><xsl:text>&#xa;</xsl:text><!--
    --></xsl:for-each>
  </xsl:when>
  <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>

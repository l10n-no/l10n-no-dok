<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="utf-8" indent="no"
            method="text"/> 

<xsl:strip-space elements="*"/>

<xsl:template match="språksamling">
---
title: Språkkodar og språknamn
author: Karl Ove Hufthammer
description: "Oversikt over norske namn (nynorsk og bokmål) på alle språka i den internasjonale standarden ISO 639-2."
lang: nn
---

Dette dokumentet inneheld oversikt over alle språka i
<a href="https://www.loc.gov/standards/iso639-2/"><abbr lang="en" title="International Organization for Standardization">ISO</abbr>&#xa0;639-2</a>,
med tilhøyrande språkkodar.

<abbr lang="en">ISO</abbr>&#xa0;639-2/<abbr lang="en" title="Bibliographic">B</abbr>
og <abbr lang="en">ISO</abbr>&#xa0;639-2/<abbr lang="en" title="Terminology">T</abbr>
er for det meste identisk. I dette dokumentet er berre
<abbr lang="en">ISO</abbr>&#xa0;639-2/<abbr lang="en">T</abbr>-koden nemnt viss
denne er forskjellig. Tilsvarande er språknamnet på bokmål berre nemnt
viss dette er forskjellig for det nynorske språknamnet. I den underliggjande
databasen er derimot alle kodar og namn eksplisitt nemnt. Ikkje rediger
dette dokumentet, men heller den underliggjande databasen. Les
kommentarane i denne for meir informasjon.

Kvalitet er mykje viktigare enn kvantitet. Alle omsetjingane
her skal vera korrekte, og er henta frå desse kjeldene hos Språkrådet:

* [Geografiske namn](https://sprakradet.no/stedsnavn-og-navn-pa-statsorgan/navnelister-norsk-skrivemate/utanlandske-stadnamn-navn-pa-stater-og-sprak-transkripsjon/)
* [Rettskrivingsendringer frå 1.&#xa0;juli 2005](https://sprakradet.no/wp-content/uploads/rettskriving2005.pdf)
* Privat korrespondanse med Språkrådet

Språka er sortert etter dei engelske språknamna. Nokre språk er
tatt med fleire gongar der engelsk har fleire ulike variantar av namnet.
Elles er alternative variantar skriven som ei nummerert og prioritert
liste. Vanlegvis vil du bruka det første namnet i lista.

Teiknet «*» er med i namn som ikkje har nokon
kjend offisiell skrivemåte. Skrivemåten er altså ikkje henta
frå kjeldene ovanfor, men frå andre (gode) kjelder.

Kommentarar kan sendast til meg, [Karl Ove Hufthammer](mailto:karl@huftis.org).

Her er statusen for omsetjinga:

Oppdatert per &lt;abbr lang="en">ISO&lt;/abbr>&#xa0;639
  ~ <xsl:value-of select="@iso639-utgåve"/>

Språk totalt
  ~ <xsl:value-of select="count(språk)"/>

Omsett til norsk
  ~ <xsl:value-of select="count(språk/nn)"/> (<xsl:number format="1" value="100 * count(språk/nn) div count(språk)"/>&#xa0;%)

&lt;table class="table">

&lt;thead>
  &lt;tr>
    &lt;th>&lt;abbr lang="en">ISO&lt;/abbr> 639-2/&lt;abbr lang="en">B&lt;/abbr>&lt;/th>
    &lt;th>&lt;abbr lang="en">ISO&lt;/abbr> 639-2/&lt;abbr lang="en">T&lt;/abbr>&lt;/th>
    &lt;th>&lt;abbr lang="en">ISO&lt;/abbr> 639-1&lt;/th>
    &lt;th>Engelsk&lt;/th>
    &lt;th>Fransk&lt;/th>
    &lt;th>Nynorsk&lt;/th>
    &lt;th>Bokmål&lt;/th>
  &lt;/tr>
&lt;/thead>

&lt;tbody>
<xsl:for-each select="(språk/en[not(alt)])|(språk/en/alt)">
<xsl:sort order="ascending" data-type="text" select="."/>
&lt;tr>
  &lt;td><xsl:value-of select="ancestor::språk/iso639-2-b"/>&lt;/td>
  &lt;td><xsl:choose>
      <xsl:when test="(ancestor::språk/iso639-2-b) != (ancestor::språk/iso639-2-t)"><xsl:value-of select="ancestor::språk/iso639-2-t"/></xsl:when>
      <xsl:otherwise>&#8211;</xsl:otherwise>
    </xsl:choose>&lt;/td>
  &lt;td><xsl:choose>
      <xsl:when test="ancestor::språk/iso639-1"><xsl:value-of select="ancestor::språk/iso639-1"/></xsl:when>
      <xsl:otherwise>&#8211;</xsl:otherwise>
    </xsl:choose>&lt;/td>
  &lt;td><xsl:apply-templates select="."/>&lt;/td>
  &lt;td><xsl:apply-templates select="ancestor::språk/fr"/>&lt;/td>
  &lt;td><xsl:apply-templates select="ancestor::språk/nn"/><xsl:if test="not(ancestor::språk/nn)">&#8211;</xsl:if>&lt;/td>
  &lt;td><xsl:choose>
      <xsl:when test="not(ancestor::språk/nb) or (ancestor::språk/nb)=(ancestor::språk/nn)">&#8211;</xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="ancestor::språk/nb"/>
      </xsl:otherwise>
    </xsl:choose>&lt;/td>
&lt;/tr>
</xsl:for-each>
&lt;/tbody>

&lt;/table>

</xsl:template>


<xsl:template match="fr|nn|nb">
<xsl:choose>
  <xsl:when test="alt">
    <xsl:for-each select="alt"><!--
    -->1. <xsl:value-of select="."/><xsl:text>&#xa;</xsl:text><!--
    --></xsl:for-each>
  </xsl:when>
  <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>

# Namna på kjelde- og målfiler
QMDFILER_AUTOGENERERT=kodar/land.qmd kodar/sprak.qmd
QMDFILER=$(shell find -name '*.qmd') $(QMDFILER_AUTOGENERERT)
HTMLFILER=$(addprefix _site/,$(QMDFILER:.qmd=.html))
CSSFILER=$(wildcard stilsett/*)

all: $(HTMLFILER)

qmd: $(QMDFILER)

_site/%.html: %.qmd _quarto.yml $(CSSFILER)
	@echo Kompilerer $<
	@quarto render $< --quiet

kodar/land.qmd: kodar/land.xml kodar/stilsett/land-til-html.xsl
	@echo Gjer om $< til $@
	@xsltproc --output $@ kodar/stilsett/land-til-html.xsl $<

kodar/sprak.qmd: kodar/sprak.xml kodar/stilsett/sprak-til-html.xsl
	@echo Gjer om $< til $@
	@xsltproc --output $@ kodar/stilsett/sprak-til-html.xsl $<

# Slett mål viss det oppstod feil ved generering (sjølv om ei fil vart generert)
.DELETE_ON_ERROR:

# Rydd opp i mellombelse filer
.PHONY: clean
clean:
	rm -f $(HTMLFILER) $(QMDFILER_AUTOGENERERT)

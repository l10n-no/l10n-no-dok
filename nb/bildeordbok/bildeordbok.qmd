---
title: "KDEs dataord: Bildeordbok"
author: Skolelinux-oversettere
description: "Norske og engelske navn på skjermelementer («widgets») i brukergrensesnitt, med bilder."
lang: nb
---

Dette er en litt beskåret kopi av KDEs
[«Visual Dictionary»](https://docs.kde.org/stable5/en/khelpcenter/fundamentals/ui.html#visualdict-widgets)
som er en hjelp for oversettere av KDE. Men bør selvsagt kunne være til hjelp for alle som oversetter dataprogrammer,
eller vil forstå dem :-)

<table class="table table-bordered">
<tr><td valign=top>en: Button<br>
no: Knapp

<td><img src="pict12.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Check Boxes<br>
no: Avkryssingsboks

<td><img src="pict13.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Colour Selector<br>
no: Fargevelger

<td><img src="pict9.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Combo Box<br>
no: Kombinasjonsboks

<td><img src="pict2.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Dialog Box<br>
no: Dialogvindu

<td><img src="pict6.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><TD Width=20%>en: Drop Down Box<br>
no: Nedtrekksliste

<td width=80%><img src="pict1.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Icon List<br>
no: Ikonliste

<td><img src="pict16.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: List Box<br>
no: Liste(boks)

<td><img src="pict7.png" align="middle" vspace="15" hspace="15" border=2><img src="pict8.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Menubar<br>
no: Menylinje

<td><img src="pict4.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Progress Bar<br>
no: Framdriftsviser

<td><img src="pict17.png" align="middle"  vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Radio Buttons<br>
no: Radioknapp

<td><img src="pict10.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Scroll Bar<br>
no: Rullefelt

<td><img src="pict18.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Slider<br>
no: Glidebryter

<td><img src="pict15.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Spinbox<br>
no: Tallboks

<td><img src="pict11.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Status Bar<br>
no: Statuslinje

<td><img src="pict20.png" align="middle"  vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Tab<br>
no: Fane

<td><img src="pict22.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Tabbed Window<br>
no: Vindu med faner

<td><img src="pict21.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Text Box<br>
no: Skrivefelt

<td><img src="pict14.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Toolbar<br>
no: Verktøylinje

<td><img src="pict3.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Tree View<br>
no: Trevisning

<td><img src="pict19.png" align="middle" vspace="15" hspace="15" border=2></td></tr>
<tr><td valign=top>en: Window Titlebar<br>
no: Tittellinje

<td><img src="pict5.png" align="middle"  vspace="15" hspace="15" border=2></td></tr>

</table>

Norsk versjon lagt ut 7. august 2003<br/>
Originalen ble laget  26. Juni 2000
